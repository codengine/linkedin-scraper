__author__ = 'codengine'

DATABASES_CONFIG = {
    'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'cwg',
            'USER': 'root',
            'PASSWORD': 'root',
            'HOST': '127.0.0.1',
            'PORT': '',
            'OPTIONS': {
                'init_command': 'SET sql_mode="STRICT_TRANS_TABLES"',
                'charset': 'utf8mb4'
            }
        }
}