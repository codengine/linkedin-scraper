from django.db import models
from django.db.models.query_utils import Q


class BaseEntityModelManager(models.Manager):
    _filter = None

    def __init__(self, *args, filter=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._filter = filter
        self.kwargs = kwargs

    def get_queryset(self):
        queryset = super().get_queryset()
        if self._filter:
            queryset = queryset.filter(Q(**self._filter))
        return queryset