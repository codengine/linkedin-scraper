from django.db import models
from django.contrib.auth.models import User
from core.manager.modelmanager.base_entity_manager import BaseEntityModelManager


class BaseEntity(models.Model):
    code = models.CharField(max_length=50)
    date_created = models.DateTimeField()
    last_updated = models.DateTimeField()
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, related_name='+', null=True)
    last_updated_by = models.ForeignKey(User, related_name='+', null=True)

    objects = BaseEntityModelManager(filter={"is_deleted": False})

    def get_code_prefix(self):
        prefix = ''.join([c for c in self.__class__.__name__ if c.isupper()])
        return prefix if prefix else self.__class__.__name__

    class Meta:
        abstract = True