from core.views.base_template_view import BaseTemplateView


class HomeView(BaseTemplateView):
    template_name = "home.html"